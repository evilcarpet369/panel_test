<?php

class Number
{
	public static function getList()
	{
		$output = array();
		$db = Db::getConnection();
		$result = $db->query("SELECT * FROM numbers ORDER BY date_time DESC");
		$result->setFetchMode(PDO::FETCH_ASSOC);
		while ($row = $result->fetch()) $output[] = $row;
		return $output;
	}


	public static function getItem($id)
	{
		$db = Db::getConnection();
		$result = $db->query("SELECT * FROM numbers WHERE id = '".$id."'");
		$result->setFetchMode(PDO::FETCH_ASSOC);
		$row = $result->fetch();
		return $row;
	}

	public static function getRemove($id)
	{
		$db = Db::getConnection();
		$db->query("DELETE FROM numbers WHERE id = '".$id."'");
	}

	public static function getSave($id, $name)
	{
		$db = Db::getConnection();
		$sql = "INSERT INTO numbers (name, date_time) VALUES ('".$name."', NOW())";
		if (!empty($id)) $sql = "UPDATE numbers SET name = '".$name."' WHERE id = '".$id."'";
		$db->query($sql);
	}
}


