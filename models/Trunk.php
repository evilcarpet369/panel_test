<?php

class Trunk
{
	public static function getList()
	{
		$output = array();
		$db = Db::getConnection();
		$result = $db->query("SELECT * FROM trunks ORDER BY date_time DESC");
		$result->setFetchMode(PDO::FETCH_ASSOC);
		while ($row = $result->fetch()) $output[] = $row;
		return $output;
	}

}


