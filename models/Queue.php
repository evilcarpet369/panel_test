<?php

class Queue
{
	public static function getList()
	{
		$output = array();
		$db = Db::getConnection();
		$result = $db->query("SELECT * FROM queues ORDER BY date_time DESC");
		$result->setFetchMode(PDO::FETCH_ASSOC);
		while ($row = $result->fetch()) $output[] = $row;
		return $output;
	}
}


