
<div class="sidebar-sticky">
    <ul class="nav flex-column px-2">
<?php
$db = Db::getConnection();
$result = $db->query("SELECT name, label FROM modules WHERE status = '1' ORDER BY sort");
$result->setFetchMode(PDO::FETCH_ASSOC);
while ($row = $result->fetch())
{
	$class = "";
	if (strpos($uri, $row['name']) !== false) $class = " active";
	if ($row['name'] == 'index') $row['name'] = "";
	echo "<li class='nav-item'><a class='nav-link".$class."' href='/panel/".$row['name']."'>".$row['label']."</a></li>";	
}
?>
    </ul>
</div>