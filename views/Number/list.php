<?php

echo "
<div class='d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2'>
	<h1 class='h2'>Номера</h1>
	<div class='btn-toolbar mb-2 mb-md-0'>
		<div class='btn-group mr-2'>
            <button type='button' class='btn btn-sm btn-outline-secondary' onclick=\"Edit('number', '0')\" data-toggle='modal' data-target='#myModal'>Добавить</button>
		</div>
	</div>
</div>
<table class='table table-striped table-sm'>
	<thead>
		<tr>
			<th>#</th>
			<th>Номер</th>
			<th>Дата</th>
			<th>Действия</th>
		</tr>
	</thead>
	<tbody>";

foreach ($List as $Item)
{
	echo "
	<tr>
		<td>".$Item['id']."</td>
		<td>".$Item['name']."</td>
		<td>".$Item['date_time']."</td>
		<td><a href='#' onclick=\"Edit('number', '".$Item['id']."')\" data-toggle='modal' data-target='#myModal'>Редактировать</a> &nbsp; <a href='#' onclick=\"Remove('number', '".$Item['id']."')\">Удалить</a></td>
	</tr>";
}

echo "
	</tbody>
</table>";
