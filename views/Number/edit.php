<?php

$header = "Добавить номер";
if (!empty($id)) $header = "Редактировать номер";

echo "
<div class='modal-dialog' role='document'>
	<div class='modal-content'>
		<form id='EditForm' onsubmit=\"Save('number'); return false\">
			<div class='modal-header'>
				<h4 class='modal-title' id='myModalLabel'>".$header."</h4> <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
	          <span aria-hidden='true'>&times;</span>
	        </button>
			</div>
			<div class='modal-body'>
				<div class='form-group'><label>Номер</label><input class='form-control' type='text' name='name' value='".@$Item['name']."' required /></div>
			</div>
				<div class='modal-footer'>
					<input type='hidden' name='id' value='".$id."' />
					<input type='submit' value='Сохранить' class='btn btn-primary' />
				</div>
			</div>
		</form>
	</div>
</div>";
