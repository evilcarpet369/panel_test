<?php

echo "
<div class='d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2'>
	<h1 class='h2'>Транки</h1>
</div>
<table class='table table-striped table-sm'>
	<thead>
		<tr>
			<th>#</th>
			<th>Номер</th>
			<th>Дата</th>
		</tr>
	</thead>
	<tbody>";

foreach ($List as $Item)
{
	echo "
	<tr>
		<td>".$Item['id']."</td>
		<td>".$Item['name']."</td>
		<td>".$Item['date_time']."</td>
	</tr>";

}

echo "
	</tbody>
</table>";
