﻿

function Edit(type, id)
{
	$.ajax({
		url: '/panel/ajax_edit_'+type+'_'+id,
		success: function(data) 
		{ 
			$('#myModal').html(data);

		}
	});
}

function Remove(type, id)
{
	$.ajax({
		url: '/panel/ajax_remove_'+type+'_'+id,
		success: function(data) 
		{ 	
			$('#main').html(data);
		}
	});
}

function Save(type)
{
	$.ajax({
       	url: '/panel/ajax_save_'+type,
        type: 'POST', 
        dataType: 'html',
        data: $('#EditForm').serialize(),  
        success: function(data) 
        { 
        	 $('#myModal').modal('toggle');
        	$('#main').html(data);
    	}
 	});
}


$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
})

