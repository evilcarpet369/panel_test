<?php include ROOT.'/views/top.php'; ?>
<div class="container-fluid">
	<div class="row">
		<nav class="col-md-2 d-none d-md-block bg-light sidebar">
		<?php include ROOT.'/views/left.php'; ?>
		</nav>
		<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4" id="main">
		[content]
		</main>
	</div>
</div>
<?php include ROOT.'/views/footer.php'; ?>