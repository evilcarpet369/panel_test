<?php

class NumberController
{
	public function actionList()
	{
		$List = Number::getList();
		require_once ROOT.'/views/Number/list.php';
	}

	public function actionEdit($params)
	{
		$id = str_replace(array($params[0], $params[1], '_', 'ajax', '/') , '', $params[2]);
		$Item = Number::getItem($id);
		require_once ROOT.'/views/Number/edit.php';
	}

	public function actionRemove($params)
	{
		$id = str_replace(array($params[0], $params[1], '_', 'ajax', '/') , '', $params[2]);
		Number::getRemove($id);
		$this->actionList();
	}

	public function actionSave($params)
	{
		foreach ($_POST as $key => $val) $$key = $val;
		Number::getSave($id, $name);
		$this->actionList();
	}
}


