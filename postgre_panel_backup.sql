PGDMP                          w            panel    11.2    11.2 "    !           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            "           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            #           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            $           1262    16393    panel    DATABASE     �   CREATE DATABASE panel WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';
    DROP DATABASE panel;
             pg_write_server_files    false            �            1259    16454    devices    TABLE     �   CREATE TABLE public.devices (
    id integer NOT NULL,
    name character varying(255),
    date_time timestamp without time zone
);
    DROP TABLE public.devices;
       public         root    false            �            1259    16452    devices_id_seq    SEQUENCE     �   CREATE SEQUENCE public.devices_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.devices_id_seq;
       public       root    false    205            %           0    0    devices_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.devices_id_seq OWNED BY public.devices.id;
            public       root    false    204            �            1259    16399    modules    TABLE     �   CREATE TABLE public.modules (
    id integer NOT NULL,
    name character varying(255),
    label character varying(255),
    sort integer,
    status integer,
    route character varying(255)
);
    DROP TABLE public.modules;
       public         root    false            �            1259    16397    modules_id_seq    SEQUENCE     �   CREATE SEQUENCE public.modules_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.modules_id_seq;
       public       root    false    197            &           0    0    modules_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.modules_id_seq OWNED BY public.modules.id;
            public       root    false    196            �            1259    16433    numbers    TABLE     �   CREATE TABLE public.numbers (
    id integer NOT NULL,
    name character varying(255),
    date_time timestamp without time zone
);
    DROP TABLE public.numbers;
       public         root    false            �            1259    16431    numbers_id_seq    SEQUENCE     �   CREATE SEQUENCE public.numbers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.numbers_id_seq;
       public       root    false    199            '           0    0    numbers_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.numbers_id_seq OWNED BY public.numbers.id;
            public       root    false    198            �            1259    16448    queues    TABLE     �   CREATE TABLE public.queues (
    id integer NOT NULL,
    name character varying(255),
    date_time timestamp without time zone
);
    DROP TABLE public.queues;
       public         root    false            �            1259    16446    queues_id_seq    SEQUENCE     �   CREATE SEQUENCE public.queues_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.queues_id_seq;
       public       root    false    203            (           0    0    queues_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.queues_id_seq OWNED BY public.queues.id;
            public       root    false    202            �            1259    16442    trunks    TABLE     �   CREATE TABLE public.trunks (
    id integer NOT NULL,
    name character varying(255),
    date_time timestamp without time zone
);
    DROP TABLE public.trunks;
       public         root    false            �            1259    16440    trunks_id_seq    SEQUENCE     �   CREATE SEQUENCE public.trunks_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.trunks_id_seq;
       public       root    false    201            )           0    0    trunks_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.trunks_id_seq OWNED BY public.trunks.id;
            public       root    false    200            �
           2604    16457 
   devices id    DEFAULT     h   ALTER TABLE ONLY public.devices ALTER COLUMN id SET DEFAULT nextval('public.devices_id_seq'::regclass);
 9   ALTER TABLE public.devices ALTER COLUMN id DROP DEFAULT;
       public       root    false    205    204    205            �
           2604    16402 
   modules id    DEFAULT     h   ALTER TABLE ONLY public.modules ALTER COLUMN id SET DEFAULT nextval('public.modules_id_seq'::regclass);
 9   ALTER TABLE public.modules ALTER COLUMN id DROP DEFAULT;
       public       root    false    197    196    197            �
           2604    16436 
   numbers id    DEFAULT     h   ALTER TABLE ONLY public.numbers ALTER COLUMN id SET DEFAULT nextval('public.numbers_id_seq'::regclass);
 9   ALTER TABLE public.numbers ALTER COLUMN id DROP DEFAULT;
       public       root    false    198    199    199            �
           2604    16451 	   queues id    DEFAULT     f   ALTER TABLE ONLY public.queues ALTER COLUMN id SET DEFAULT nextval('public.queues_id_seq'::regclass);
 8   ALTER TABLE public.queues ALTER COLUMN id DROP DEFAULT;
       public       root    false    203    202    203            �
           2604    16445 	   trunks id    DEFAULT     f   ALTER TABLE ONLY public.trunks ALTER COLUMN id SET DEFAULT nextval('public.trunks_id_seq'::regclass);
 8   ALTER TABLE public.trunks ALTER COLUMN id DROP DEFAULT;
       public       root    false    200    201    201                      0    16454    devices 
   TABLE DATA               6   COPY public.devices (id, name, date_time) FROM stdin;
    public       root    false    205   c                  0    16399    modules 
   TABLE DATA               G   COPY public.modules (id, name, label, sort, status, route) FROM stdin;
    public       root    false    197   �                  0    16433    numbers 
   TABLE DATA               6   COPY public.numbers (id, name, date_time) FROM stdin;
    public       root    false    199   �!                 0    16448    queues 
   TABLE DATA               5   COPY public.queues (id, name, date_time) FROM stdin;
    public       root    false    203   "                 0    16442    trunks 
   TABLE DATA               5   COPY public.trunks (id, name, date_time) FROM stdin;
    public       root    false    201   �"       *           0    0    devices_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.devices_id_seq', 1, false);
            public       root    false    204            +           0    0    modules_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.modules_id_seq', 21, true);
            public       root    false    196            ,           0    0    numbers_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.numbers_id_seq', 6, true);
            public       root    false    198            -           0    0    queues_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.queues_id_seq', 5, true);
            public       root    false    202            .           0    0    trunks_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.trunks_id_seq', 1, false);
            public       root    false    200                  x������ � �         �   x�}�KN1D��S�O>pv��FB����(�XBbÒ-���HC�9C�F�{��U��~U]e�MbsExB���5J�<I195i��f��]��"��+�T�aL�գ������h�r���-���O�`�.K(h����q��/��Cr��
S�-�ݽD\��G��K�)5��ܜ4�	o���F�9Z�:T�}�P��(}@&��V����q���Ć�TҊ����n*Ҕ��Z�vw�w��"5�SJ�u��#         b   x�m���0��Y��@�R<T���#~Ā��?�A��J;��C}(O��ڌ�Y�������kkMZt	^!��C봵���iٙ�nh�B��)"x[o         y   x�3�44���1�q�+.O-�420��50"#C+#+#=sCsKss.#�R�����T�������SM|��9M�,-u�u�3K4��5��a�l�1��B,1��H�K/-��4F��� g8�            x������ � �     