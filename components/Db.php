<?php

class Db
{
	public static function getConnection()
	{
		$dbhost = 'localhost';
		$dbname = 'panel';
		$dbuser = 'root';
		$dbpassword = '1234';

		$db = new PDO("pgsql:host=".$dbhost.";port=5432;dbname=".$dbname.";user=".$dbuser.";password=".$dbpassword);
		$db->exec("SET CHARACTER SET utf8");
		return $db;
	}
}