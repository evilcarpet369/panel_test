<?php

class Router 
{ 
	private $routes;

	public function __construct()
	{
		// DB
		$db = Db::getConnection();
		$arr_routes = array();
		$result = $db->query("SELECT name, route FROM modules ORDER BY sort");
		$result->setFetchMode(PDO::FETCH_ASSOC);
		while ($row = $result->fetch()) $arr_routes[$row['name']] = $row['route'];

		$this->routes = $arr_routes;
	}

	public function getURI()
	{
		$output = trim($_SERVER['REQUEST_URI'], '/');
		$output = str_replace('panel', '', $output);
		if (empty($output)) $output = "index";
		return $output;
	}

	public function run()
	{
		$uri = $this->getURI();
		
		foreach ($this->routes as $uriPattern => $path)
		{
			if (preg_match("~".$uriPattern."~", $uri))
			{
				$segments = explode('/', $path);
				$controllerName = ucfirst($segments[0]).'Controller';
				$actionName = 'action'.ucfirst($segments[1]);

				$params = $segments;
				$params[] = $uri;

				$controllerFile = ROOT.'/controllers/'.$controllerName.'.php';
				if (file_exists($controllerFile)) include_once $controllerFile;

				$controllerObject = new $controllerName;

				ob_start();
				$result = $controllerObject->$actionName($params);
				$output = ob_get_contents();
				ob_end_clean();

				if ($result != null) break;
			}
		}

		Router::Show($output, $uri);
	}


	public static function Show($output, $uri)
	{
		$content = "[content]";
		if (strpos($uri, 'ajax_') == false)
		{
			ob_start();
			include_once ROOT."/views/index.php";
			$content = ob_get_contents();
			ob_end_clean();	
		}
		
		$content = str_replace(array("[content]"), array($output), $content);
		echo $content;
	}
}